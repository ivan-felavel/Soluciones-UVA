#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

int n, m, s;
char mat[105][105];

//D right
//E left
//L east
//O west

char change_orientation(char a, char c)
{
	if (c == 'E') 
	{
		if (a == 'N') return  'O';
		if (a == 'S') return  'L';
		if (a == 'L') return  'N';
		if (a == 'O') return  'S';
	}
	if (a == 'N') return  'L';
	if (a == 'S') return  'O';
	if (a == 'L') return  'S';
	if (a == 'O') return  'N';
}


bool check (int i, int j)
{
	if (i < 0 || i >= n || j < 0 || j >= m || mat[i][j] == '#')
		return false;
	return true;
}

int main(int argc, char const *argv[])
{
	while (cin >> n >> m >> s && n != 0)
	{
		memset(mat, '-', sizeof(mat));
		int indexj = 0;
		int indexi = 0;
		string instructions;
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < m; ++j)
			{	
				cin >> mat[i][j]; 
				mat[i][j] != '*' && mat[i][j] != '.' && mat[i][j] != '#' ? indexi = i, indexj = j : false;
			}

		for (int  i = 0; i< n; i++)
		{
			for (int j = 0; j < m; ++j)
			{
				cout << mat[i][j];
			}
			cout << endl;
		}

		cin >> instructions;
		int stickers = 0;
		for (int i = 0; i < instructions.size(); ++i)
		{
			bool flag = true;
			//cout << mat[indexi][indexj] << endl;
			if (instructions[i] == 'F')
			{
				if (mat[indexi][indexj] == 'N' && flag)
					if (check(indexi-1, indexj)) 
						{
							mat[indexi][indexj] = '.';
							if (mat[indexi-1][indexj] == '*') stickers++; 
							indexi--; 
							mat[indexi][indexj] = 'N';
							flag = false;
						}
				if (mat[indexi][indexj] == 'S' && flag)
					if (check(indexi+1, indexj)) 
						{
							mat[indexi][indexj] = '.';
							if (mat[indexi+1][indexj] == '*') stickers++; 
							indexi++; 
							mat[indexi][indexj] = 'S';
							flag = false;
						}
				if (mat[indexi][indexj] == 'L' && flag)
					if (check(indexi, indexj+1)) 
						{
							mat[indexi][indexj] = '.';
							if (mat[indexi][indexj+1] == '*') {stickers++;} 
							indexj++; 
							mat[indexi][indexj] = 'L';
							flag = false;
						}
				if (mat[indexi][indexj] == 'O' && flag)
					if (check(indexi, indexj-1)) 
						{
							mat[indexi][indexj] = '.';
							if (mat[indexi][indexj-1] == '*') {stickers++;} 
							indexj--; 
							mat[indexi][indexj] = 'O';
							flag = false;
						}

			}
			else
				mat[indexi][indexj] = change_orientation(mat[indexi][indexj], instructions[i]);	
		}
		cout << stickers << endl;

	}

	return 0;
}