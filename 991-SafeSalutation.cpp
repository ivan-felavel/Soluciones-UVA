#include <cstdio>
#include <iostream>
#include <cstring>
using namespace std;

long long f[35];
long long factorial(long long n) {
	if (n <= 1) return 1;
	if (f[n] != -1) return f[n];
	return f[n] = n * factorial(n - 1);
}
	 	
int main(int argc, char const *argv[])
{
	memset(f, -1, sizeof f);
	int n;
	long long ans;
	bool flag = true;
	while(scanf("%d", &n) != EOF) {
		ans = factorial(2 * n) / (factorial(n) * factorial(n + 1));
		if (not flag)
			printf("\n");
		printf("%lld\n", ans);
		flag = false;
	}
	return 0;
}