#include <iostream>
#include <vector>
#include <cstdio>
#include <map>
using namespace std;

int t, n;
vector<int> x;
vector<int> vectorSuma;
int xi;
map<vector<int>, bool> resultados;

void imprimeVector (const vector<int> &v) {
	for (int i = 0; i < v.size(); ++i)
	{
		if (i > 0) cout << "+";
		cout << v[i];
	}
	cout << endl;
}

void backtracking(int i, int suma) {
	if (i >= n) { 
		if (suma == t){
			resultados[vectorSuma];
				if (!resultados[vectorSuma])
					imprimeVector(vectorSuma);
				resultados[vectorSuma] = true;
		}
		return;
	}
	if(suma > t) {
		return;
	}
	suma += x[i];
	vectorSuma.push_back(x[i]);
	backtracking(i + 1, suma);
	vectorSuma.pop_back();
	suma -= x[i];
	backtracking(i + 1, suma);
}


int main(int argc, char const *argv[])
{
	ios::sync_with_stdio(false);
	while((cin >> t and cin >> n) and (t and n)){
		x.resize(n);
		resultados.clear();
		for (int i = 0; i < n; ++i)
			cin >> x[i];

		cout << "Sums of " << t << ":" << endl;
		backtracking(0, 0);
		if (resultados.empty())
			cout << "NONE" << endl;
	}
	return 0;
}
