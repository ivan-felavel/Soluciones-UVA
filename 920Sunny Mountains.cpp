#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <vector>
using namespace std;


double dist(int x1, int x2, int y1, int y2){
	return hypot(x1 - x2, y1 - y2);
}

int main(int argc, char const **argv)
{
	int c;
	scanf("%d", &c);
	while(c--)
	{
		int n;
		scanf("%d", &n);
		vector <pair<int, int>> coordinates;
		for (int i = 0; i < n; ++i)
		{
			int x,y;
			scanf("%d %d", &x, &y);
			coordinates.push_back(make_pair(x,y));
		}

		sort(coordinates.begin(), coordinates.end());
		reverse(coordinates.begin(), coordinates.end());

		double distance = 0.0;
		int x1 = 0;
		int x2 = 0;
		int y1 = 0;
		int y2 = 0;
		int ymax = 0;

		for (int i = 1; i < coordinates.size(); i++)
		{
			if (coordinates[i].second > ymax)
			{
				y1 =  coordinates[i].second;
				x1 = coordinates[i].first;
				y2 =  coordinates[i-1].second;
				x2 = coordinates[i-1].first;
 				distance += dist(x1, x2, y1, y2) * ((double)(y1 - ymax) /(double) (y1 - y2));
				ymax = y1;
			}

		}
		cout << fixed << setprecision(2) << distance << endl;




	}
	return 0;
}