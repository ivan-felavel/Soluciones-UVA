#include <iostream>
#include <vector>
#include <bitset>
#include <cmath>
using namespace std;

long long _sieve_size;
bitset<10000010> bs;
vector <int> primes;

void criba(long long upperbound)
{
	_sieve_size = upperbound + 1;
	bs.set();
	bs[0] = bs[1] = 0;
	for (long long i = 2; i <= _sieve_size; i++)
	{
		if (bs[i])
		{
			for (long long j = i * i; j <= _sieve_size; j += i) bs[j] = 0;
			primes.push_back((int)i);
		}
	}
}

bool isPrime(long long N)
{
	if (N <= _sieve_size) return bs[N];
	for (int i = 0; i < (int) primes.size(); i++)
		if (N % primes[i] == 0) return false;
	return true;
}


int main(int argc, char const *argv[])
{
	criba(46350);
	long long n;
	while(scanf("%lld", &n) && n != 0)
	{
		bool primero = true;
		printf("%lld =", n);
		if (n < 0) {n = abs(n); printf(" -1 x");}
		if (isPrime(n)) 
			printf(" %lld", n); 
		else
		{
			for (int i = 0; i < (int)primes.size(); ++i)
			{
				if (n <= 1) break;
				while(n % primes[i] == 0 && n)
				{
					if (!primero)
						printf(" x");
					printf(" %d",primes[i]);
					primero = false;
					n /= primes[i]; 
				}
			}
		}
		printf("\n");
	}

	return 0;
}