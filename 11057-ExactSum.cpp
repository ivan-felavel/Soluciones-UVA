#include <algorithm>
#include <cstdio>
#include <vector>
#include <iostream>
#include <cmath>
using namespace std;

int main(int argc, char const *argv[])
{
	int n;
	int m;
	int book_price;
	vector<int> prices;
	while(scanf("%d", &n) != EOF) {
		prices.clear();
		for (int i = 0; i < n; ++i) {
			scanf("%d", &book_price);
			prices.push_back(book_price);
		}
		scanf("%d", &m);
		sort(prices.begin(), prices.end());
		int ans_i, ans_j;
		int actual_diff = 10000011;
		int equal_counter = 0;
		for (int i = 0; i < prices.size(); ++i)
		{
			if (binary_search(prices.begin(), prices.end(), m - prices[i]))
			{
				abs(prices[i] - (m - prices[i])) == 0 ? equal_counter++ : false;
				if (abs(prices[i] - (m - prices[i])) < actual_diff)
				{
					if (abs(prices[i] - (m - prices[i])) == 0 || equal_counter == 1)
					{
						continue;
					}
					ans_i = prices[i];
					ans_j = m - prices[i];
					actual_diff = abs(prices[i] - (m - prices[i]));
				}
			}
		}
		printf("Peter should buy books whose prices are %d and %d.\n\n", ans_i, ans_j);

	}
	return 0;
}