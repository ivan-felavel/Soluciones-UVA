#include <cstdio>
#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	ios::sync_with_stdio(false);
	int N;
	int ni;
	while(cin >> N && N) {
		int suma = 0;
		int ans = 0;
		while(N--) {
			cin >> ni;
			suma += ni;
			ans = max(ans, suma);
			if (suma < 0) suma = 0;
		}
		if (ans > 0)
			cout << "The maximum winning streak is " << ans << "." << endl;
		else
			cout << "Losing streak." << endl;
	}

	return 0;
}		