import sys

for line in sys.stdin:
	A = list(map(int, line.strip().split(" ")))
	producto = 1
	ans = -999999
	for i in range(len(A) - 2):
		producto = 1
		for j in range(i + 1, len(A) - 1):
			producto *= A[j]
			ans =  max(ans, producto)
			if producto < -999999**100:
				producto = 1
	if ans == -999999**100:
		ans = A[0]
	print (ans)