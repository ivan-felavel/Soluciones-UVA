#include <cstdio>

int main(int argc, char const *argv[])
{
	int routes;
	int s;
	int ni;
	int suma;
	int ans;
	int index_j_actual = 0;
	int index_i_actual = 0;
	int index_i_max = 0;
	int id_route = 1;
	scanf("%d", &routes);
	while(routes--) {
		index_j_actual = 0;
		index_i_actual = 0;
		index_i_max = 0;
		scanf("%d", &s);
		suma = 0;
		ans = 0;
		for (int i = 1; i < s; ++i) {
			scanf("%d", &ni);
			suma += ni;
			
			if (ans < suma || (ans == suma && index_j_actual - index_i_max < i - index_i_actual)) {
				index_i_max = index_i_actual;
				index_j_actual = i;
				ans = suma;
			}		
			if (suma < 0) {
				suma = 0;
				index_i_actual = i;
			}
		}
		if (ans == 0)
			printf("Route %d has no nice parts\n", id_route++);
		else {
			printf("The nicest part of route %d is between stops %d and %d\n", id_route++, index_i_max + 1, index_j_actual + 1);
		} 

	}


	return 0;
}