#include <iostream>
#include <algorithm>
#include <cmath>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
	int n, m, a, b, c;
	while (cin >> n >> m && n)
	{
		vector <int> teen;
		int contador = 0;
		vector <pair <pair<int, int>, int>> restricciones(m);
		for (int i = 0; i < n; ++i) {teen.push_back(i);}
		for (int i = 0; i < m; ++i)
		{
			cin >> a >> b >> c;
			restricciones.push_back(make_pair(make_pair(a,b), c));
		}
		bool flag = true;
		do
		{
			int index_a = 0;
			int index_b = 0;
			flag = true;
			//Para cada restricción	
			for (int i = 0; i < restricciones.size(); ++i)
			{
				//Checar en el array de teenagers
				for (int j = 0; j < teen.size() ; ++j)
				{
					if (restricciones[i].first.first == teen[j])
						index_a =  j;
					if (restricciones[i].first.second ==  teen[j])
						index_b = j;
				}

				if (restricciones[i].second < 0)
				{
					if (abs(index_a - index_b) < abs(restricciones[i].second))
					{
						flag = false;
						break;
					}
				}
				else if (restricciones[i].second >= 0)
					if (abs(index_b-index_a) > abs(restricciones[i].second))	
					{
						flag = false;
						break;
					}
			}
			flag ? contador++ : false;
		} while (next_permutation(teen.begin(), teen.end()));

		cout << contador << endl;

	}
	return 0;
}