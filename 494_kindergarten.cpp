#include <iostream>
#include <string>
#include <cctype>
using namespace std;

int main(int argc, char const *argv[])
{
	string cadena;
	while(getline(cin, cadena))
	{
		int palabras = 0;
		bool es_palabra = 0;
		for (int i = 0; i < cadena.size(); ++i)
		{
			if (isalpha(cadena[i]))
				es_palabra = 1;
			else
			{
				palabras += es_palabra;
				es_palabra = 0;
			}
		}
		palabras += es_palabra;
		cout << palabras << endl;
	}
	return 0;
}