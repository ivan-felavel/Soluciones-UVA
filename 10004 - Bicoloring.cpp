#include <iostream>
#include <vector>
#include <cstring>
#include <queue>
using namespace std;

typedef vector <int> vi;


int main(int argc, char const *argv[])
{
	int n;
	int m;
	while(cin >> n, n)
	{
		int color[205];
		memset(color, -1 , sizeof(color));
		cin >> m;
		vector <vi> ListaAdj;
		int a,b;
		ListaAdj.clear();
		ListaAdj.resize(205);
		for (int i = 0; i < m; i++)
		{
			cin >> a >> b;
			ListaAdj[a].push_back(b);  
			ListaAdj[b].push_back(a); 
		}
		queue <int> Q;
		Q.push(0);
		color[0] = 0;
		bool isBipartite = true;
		while (!Q.empty())
		{
			int u = Q.front();
			Q.pop();
			for (int i = 0; i < ListaAdj[u].size(); ++i)
			{
				int v = ListaAdj[u][i];
				if (color[v] == -1)
				{
					color[v] = 1 - color[u];
					Q.push(v);
				}
				else if(color[u] == color[v])
				{
					isBipartite = false;
					break;
				}
			}
		}

		if (!isBipartite)
			cout << "NOT BICOLORABLE." << endl;
		else 
			cout << "BICOLORABLE." << endl;
	}

	return 0;
}