#include <iostream>
#include <vector>
using namespace std;

typedef vector <double> vf;


int main(int argc, char const *argv[])
{
	int months, depRecords;
	double downPayment, loanAmount;
	while(cin>>months and months>0)
	{
		double aux;
		int k;
		cin>>downPayment>>loanAmount>>depRecords;
		vf depreciation (months+1,-1);
		for(int i=0; i<depRecords; i++)
		{
			cin>>k;
			cin>>aux;
			depreciation[k]=aux;
		}

		for (int i=1; i< depreciation.size(); i++)
			if(depreciation[i]==-1)
				depreciation[i]=depreciation[i-1];
		//cout<<"----------------"<<endl;
		//for(int i=0; i<depreciation.size();i++)
		//	cout<<i<<" "<<depreciation[i]<<endl;

		//cout<<"----------------"<<endl;
		double carValue= 0.0;
		carValue=downPayment + loanAmount;
		
		double monthPayment;
		monthPayment=loanAmount/months;
		int i;
		for(i=0 ; i< months ; i++)
		{

			carValue -= carValue * depreciation[i];
			
		//	cout<<carValue<< " "<< loanAmount<<endl;
			if( loanAmount <carValue )
			{
				break;
			}
			loanAmount -= monthPayment;
		}
		if(i==1)
			cout<<"1 month"<<endl;
		else 
			cout<<i<<" months"<<endl;
	}
	return 0;
}