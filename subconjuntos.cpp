#include <iostream>
#include <bitset>
using namespace std;


int main(int argc, char const *argv[])
{
	int n;
	while(cin >> n && n)
	{
		for (int i = 0; i < 1 << n; ++i)
		{
			bitset<32> b(i);
			if (!b.count()) cout << "0";
			for (int j = 0; j < 32; ++j)
			{
				if (b[j]) cout << j + 1 << " "; 
			}
			cout << endl;
		}
	}
	return 0;
}