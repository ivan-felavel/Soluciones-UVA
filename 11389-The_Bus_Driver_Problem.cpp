#include <cstdio>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;

bool f(int a, int b) {
	return a > b;
}

int main(int argc, char const *argv[])
{
	int n, d, r;
	vector<int> morning_lengths;
	vector<int> night_lengths;
	int diff = 0;
	int ai;
	while(scanf("%d %d %d", &n, &d, &r) && n != 0) {
		diff = 0;
		morning_lengths.clear();
		night_lengths.clear();
		for (int i = 0; i < n; ++i)
		{
			scanf("%d", &ai);
			morning_lengths.push_back(ai);
		}
		for (int i = 0; i < n; ++i)
		{
			scanf("%d", &ai);
			night_lengths.push_back(ai);
		}

		sort(morning_lengths.begin(), morning_lengths.end());
		sort(night_lengths.begin(), night_lengths.end(), f);
		for (int i = 0; i < n; ++i)
			if ((morning_lengths[i] + night_lengths[i]) > d)
				diff += abs(d - (morning_lengths[i] + night_lengths[i]))	;
		printf("%d\n", diff * r);
	}
	return 0;
}