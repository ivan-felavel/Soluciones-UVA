#include <iostream>
#include <string>
using namespace std;

char reverse(char c)
{
	if(c == 'A') return 'A';
	if(c == 'E') return '3';
	if(c == 'H') return 'H';
	if(c == 'I') return 'I';
	if(c == 'J') return 'L';
	if(c == 'L') return 'J';
	if(c == 'M') return 'M';
	if(c == 'O') return 'O';
	if(c == 'S') return '2';
	if(c == 'T') return 'T';
	if(c == 'U') return 'U';
	if(c == 'V') return 'V';
	if(c == 'W') return 'W';
	if(c == 'X') return 'X';
	if(c == 'Y') return 'Y';
	if(c == 'Z') return '5';
	if(c == '1') return '1';
	if(c == '2') return 'S';
	if(c == '3') return 'E';
	if(c == '5') return 'Z';
	if(c == '8') return '8';
	return '#';
}

int main(int argc, char const *argv[])
{
	string s;
	
	while(getline(cin, s))
	{
		bool mirrored_pal = true;
		for (int i = 0, j = s.size() - 1; i < s.size() / 2; i++, j--)
			if (!(s[i] == s[j] and reverse(s[i]) == s[i] and reverse(s[j]) == s[j]))
			{ mirrored_pal = false;	break; }

		if (mirrored_pal)
			if (s[s.size() / 2] != reverse(s[s.size() / 2]))
				mirrored_pal = false;
		
		bool palindromo = true;

		if (!mirrored_pal)
			for (int i = 0, j = s.size() - 1; i < s.size() / 2; i++, j--)
				if (s[i] != s[j]) { palindromo = false; break; }

		bool mirrored = true;
		if (!palindromo)
			for (int i = 0, j = s.size() - 1; i < s.size() / 2; i++, j--)
				if (reverse(s[i]) != s[j]) { mirrored = false; break; } 

		if (mirrored_pal) cout << s <<" -- is a mirrored palindrome." << endl;
		else if (palindromo) cout << s <<" -- is a regular palindrome." << endl;
		else if (mirrored) cout << s <<" -- is a mirrored string." << endl;
		else cout << s << " -- is not a palindrome." << endl;
		cout << endl;
	}
	return 0;
}