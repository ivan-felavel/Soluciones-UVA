#include <iostream>
#include <vector>
#include <string>
#include <bitset>
using namespace std;

typedef vector <int> vi;
bitset <100> visita;

vector <vi> ListaAdj;

void dfs(int v)
{
	int u;
	visita[v] = 1; //marcamos el nodo
	for(int j = 0; j < ListaAdj[v].size(); j++) //para cada vecino de v
	{
		u = ListaAdj[v][j]; // si el j-ésimo vecino no está visitado comezamos una dfs
		if(!visita[u])
			dfs(u);
	}
}

int main(int argc, char const *argv[])
{

	int t;
	int space;
	cin >> t;
	string s,a;
	getline(cin, a);
	getline(cin, a);

	for (int i = 0; i < t; ++i)
	{
		visita.reset();
		int numeroV = 0;
		ListaAdj.clear();
		ListaAdj.resize(100);
		while(getline(cin, s) and !s.empty())
		{
			if (numeroV == 0)
			{
				numeroV = s[0] - 'A';
			}
			for (auto v: s)
			{
				//cout << s[0] << v << endl;
				if (v != s[0])
				{
					ListaAdj[s[0] - 'A'].push_back(v - 'A'); 
					ListaAdj[v - 'A'].push_back(s[0] - 'A'); 
				}
			}

		}
        int numCC = 0;
		for (int i = 0; i <= numeroV; ++i)
            if (!visita[i])
              	++numCC, dfs(i);
        cout << numCC << endl;
        if (i < t - 1)
        {
        	cout << endl;
        }
	}
	return 0;
}