#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
	int routes;
	int r = 0;
	scanf("%d", &routes);
	while(routes--)
	{
		int n;
		scanf("%d",&n);
		int sum = 0;
		int index_sup;
		int index_inf = 0;
		int maximo = 0;
		vector <int> a(n, 0);
		vector <int> acumulador(n, 0);
		vector <pair<int, int>> intervalos;

		a[0] = -1;
		acumulador[0] = -1;

		for (int i = 1; i < n; ++i)
			scanf("%d",&a[i]);
		for (int i = 1; i < n; ++i)
		{

			if (sum + a[i] > 0) sum += a[i], acumulador[i] = sum; 
			else
			{
			 	acumulador[i] = sum + a[i];
			 	index_inf = i;
			 	sum = 0;
			}
			if(maximo < acumulador[i])
			{
				index_sup = i; maximo = acumulador[i]; intervalos.push_back(make_pair(index_inf, index_sup));
			}
		}
		pair <int, int> int_max;
		int dif = 0;
		for (auto p: intervalos)
		{
			if (abs(p.second - p.first) > dif)
			{
				int_max = p;
				dif = abs(p.second - p.first);  
			}
		}

		if (maximo)
			printf("The nicest part of route %d is between stops %d and %d\n",++r, int_max.first + 1, int_max.second + 1);
		else
			printf("Route %d has no nice parts\n", ++r);
	}
	return 0;
}