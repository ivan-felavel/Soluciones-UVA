#include <iostream>
#include <string>
#include <cstdio>
#include <bitset>
using namespace std;

int main(int argc, char const *argv[])
{
	int round;
	string s;
	string s2;
	while(cin>>round and round != -1)
	{
		getline (cin, s);
		getline (cin, s);
		getline (cin, s2);
		int count1=0;
		int count2=0;
		bitset <27> guessWord;
		bitset <27> tryWord;
		guessWord.reset();
		tryWord.reset();
		int hangman=3;
		int stroke=0;
		for(int i=0; i< s.size(); i++)
			if(guessWord[s[i]-97]==0){guessWord[s[i]-97]=1; count1++;}
		for (int i = 0; i < s2.size(); ++i)
		{
			if(guessWord[s2[i]-97]==1)
			{
				if(tryWord[s2[i]-97]==0)
				{
					tryWord[s2[i]-97]=1;
					count2++;
				}
			}
			if(count2==count1)
			{
				hangman=0;
				break;
			}
			if (guessWord[s2[i]-97]!=1)
			{
				stroke++;
				if (stroke==7)
				{
					hangman=1;
					break;
				}
			}
		}

		cout<<"Round "<<round<<endl;
		
		if (hangman==3)
			cout<<"You chickened out."<<endl;
		if (hangman==0)
			cout<<"You win."<<endl;
		if (hangman==1)
			cout<<"You lose."<<endl;
	}
	return 0;
}