#include <iostream>
#include <vector>
#include <cstdio>
using namespace std;

int main(int argc, char const *argv[])
{
	int n;
	while(scanf("%d", &n) && n)
	{
		vector <int> carros (n);
		vector <int> posiciones (n);
		vector <int> config_inicial (n, -1);
		bool es_posible = true;
		for (int i = 0; i < n; ++i)
			scanf("%d %d", &carros[i], &posiciones[i]);
		for (int i = 0; i < n; i++)
		{
			if (config_inicial[i + posiciones[i]] == -1 && i + posiciones[i] < n && i + posiciones[i] >= 0)
				config_inicial[i + posiciones[i]] = carros[i];
			else 
			{
				es_posible = false;
				break;
			}
		}
		if (!es_posible)
			printf("-1");
		else
			for (int i = 0; i < n; ++i)
			{	
				if (i)
					printf(" ");
				printf("%d", config_inicial[i]);
			}
		printf("\n");
	}
	return 0;
}