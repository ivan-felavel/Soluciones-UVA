#include <map>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <iomanip>
using namespace std;

int main(int argc, char const *argv[])
{
	int n,is;
	string tree;
	cin >> n;
	getline(cin, tree);
	//getline(cin, tree);	
	while(n--)
	{
		//cout << n << endl;
		map <string, int> hardwood;
		int total = 0;

		while(getline(cin, tree) and tree != "")
		{
			//cout << tree << endl;
			++hardwood[tree];
			++total;
		}
		for(auto par : hardwood)
		{
			printf("%s %0.4f\n", par.first.c_str(), (double)par.second / (double)total *  100.0);
		}
		if (n > 0)
			cout << endl;

	}

	return 0;
}