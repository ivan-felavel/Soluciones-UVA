#include <bits/stdc++.h>
using namespace std;

typedef vector <int> vi;

int main(int argc, char const *argv[])
{
	int n;
	vi v;
	while(cin>>n and n!=-1)
	{
		int suma=0;
		for(int i=1;suma<=n;i++)
		{
			suma+=i;
			if(suma<n)
			{
				v.push_back(i);
			}
			else
			{
				suma-=v[0];
				v.erase(v.begin());
				
			}
		}
		cout<<v[0]<<"+...+"<<v[v.size()-1];
	}
	return 0;
}