#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

int main(int argc, char const *argv[])
{
	int t;
	cin >> t;
	while(t--)
	{
		long long n, i = 0;
		string s;
		cin >> s >> n;
		do
		{
			++i;
		}while(next_permutation(s.begin(), s.end()) && i < n);
		cout << s << endl;
	}
	return 0;
}