/*#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
using namespace std;


char reverse (char a)
{
	if(a=='A')
		return 'A';
	if(a=='E')
		return '3';
	if(a=='H')
		return 'H';
	if(a=='I')
		return 'I';
	if(a=='J')
		return 'L';
	if(a=='L')
		return 'J';
	if(a=='M')
		return 'M';
	if(a=='O')
		return 'O';
	if(a=='S')
		return '2';
	if(a=='T')
		return 'T';
	if(a=='U')
		return 'U';
	if(a=='V')
		return 'V';
	if(a=='W')
		return 'W';
	if(a=='X')
		return 'X';
	if(a=='Y')
		return 'Y';
	if(a=='Z')
		return '5';
	if(a=='1')
		return '1';
	if(a=='2')
		return 'S';
	if(a=='3')
		return 'E';
	if(a=='5')
		return 'Z';
	if(a=='8')
		return '8';
	return '-';	
}


int main(int argc, char const *argv[])
{
	char ss[21];
	string s;
	bool k=true;
	while(cin>>s)
	{
		if(!k)cout<<endl;
		k=false;
		bool palindrome=1;
		bool mirror=palindrome;
		for (int i = 0, j=s.size()-1; i < s.size()/2; i++, j--)
			{
				s[i]!=s[j] ?  palindrome=0: 1;
				reverse(s[i])!=s[j] ? mirror=0: 1;
			}
			if(palindrome and mirror)
				cout<<s<<" -- is a mirrored palindrome."<<endl;
			else if(palindrome and !mirror)
				cout<<s<<" -- is a regular palindrome."	<<endl;
			else if(!palindrome and mirror)
				cout<<s<<" -- is a mirrored string."<<endl;
			else cout<<s<<" -- is not a palindrome."<<endl;
	}
	return 0;
}		

*/

#include <cstdio>
#include <cstring>
#include <stack>
#include <map>
using namespace std;

int main() {
	char s[21];
	map<char, char> rev;
	rev['A'] = 'A';
	rev['H'] = 'H';
	rev['I'] = 'I';
	rev['E'] = '3';
	rev['J'] = 'L';
	rev['L'] = 'J';
	rev['M'] = 'M';
	rev['O'] = 'O';
	rev['T'] = 'T';
	rev['U'] = 'U';
	rev['V'] = 'V';
	rev['W'] = 'W';
	rev['X'] = 'X';
	rev['S'] = '2';
	rev['Y'] = 'Y';
	rev['Z'] = '5';
	rev['1'] = '1';
	rev['2'] = 'S';
	rev['3'] = 'E';
	rev['5'] = 'Z';
	rev['8'] = '8';

	while (scanf("%s", s) != EOF) {
		stack<char> checker;
		int len = strlen(s);

		bool palin = true, mirror = true;

		int pos = 0;
		for (; pos < len / 2; pos++) {
			checker.push(s[pos]);
		}
		if (len % 2 != 0) {
			if (!rev.count(s[pos]))
				mirror = false;
			if (rev[s[pos]] != s[pos])
				mirror = false;
			pos++;
		}

		for (; pos < len; pos++) {
			if (checker.top() != s[pos]) {
				palin = false;
			}
			if (!rev.count(checker.top()) || rev[checker.top()] != s[pos]) {
				mirror = false;
			}
			checker.pop();
		}

		printf("%s -- ", s);
		if (palin && mirror)
			printf("is a mirrored palindrome.");
		else if (palin && !mirror)
			printf("is a regular palindrome.");
		else if (!palin && mirror)
			printf("is a mirrored string.");
		else if (!palin && !mirror)
			printf("is not a palindrome.");
		printf("\n\n");
	}

	return 0;
}