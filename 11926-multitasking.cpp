#include <iostream>
#include <cstdio>
#include <bitset>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
	int n, m;
	int start, finish, interval;
	int i = 0;
	vector<bitset<1000005>> one_time_tasks (105);
	vector<bitset<1000005>> repeating_tasks (105);
	while(scanf("%d %d",  &n, &m) != EOF){
		for (i = 0; i < n; ++i)
		{
			scanf("%d %d", &start, &finish);
			one_time_tasks[i].reset();
			one_time_tasks[i][start] = 1;
			one_time_tasks[i][finish] = 1;
		}
		for (i = 0; i < m; ++i)
		{
			scanf("%d %d %d", &start, &finish, &interval);
			repeating_tasks[i].reset();
			repeating_tasks[i][start] = 1;
			repeating_tasks[i][finish] = 1;
			int index_start = start, index_finish = finish;
			for (int j = 1; index_finish < repeating_tasks[i].size();index_finish = (finish + interval * j), index_start = (start + interval * j), j++)
			{
				repeating_tasks[i][index_finish] = 1;	
				repeating_tasks[i][index_start] = 1;	
			}
		}
		int total_tasks = n + m;

		

	}
	return 0;
}