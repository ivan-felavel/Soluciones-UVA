#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
	int t,a,b,c;
	cin>>t;
	while(t--)
	{
		cin>>a>>b>>c;
		int pos1;
		int pos2;
		int tiro;
		vector <int>  jugadores (a,1);
		vector <int> tiroDados (c);
		vector <int> tablero (105);
		for(int i=1; i<= tablero.size(); i++)
			tablero[i]=i;
		for(int i=0; i<b; i++)
		{
			cin>>pos1>>pos2;
			tablero[pos1]=pos2;
		}
		for (int i = 0; i < c; i++)
		{
			//turno del jugador i+1%c;
			cin>>tiro;
			cout<<" pos jug "<<i%c<<" "<<jugadores[i%c]<<endl;
			cout<<tablero[jugadores[i%c]+tiro +1]<<endl;
			jugadores[i%c]=(tablero[jugadores[i%c]+tiro +1]>=100 ? 100 : tablero[jugadores[i%c]+tiro +1] );
		}
		for(int i=0; i<a ; i++)
		{
			cout<<"Position of player "<<i+1<<" is "<<jugadores[i]<<"."<<endl;
		}
	} 
	return 0;
}