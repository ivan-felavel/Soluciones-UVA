#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char const *argv[])
{
	vector <int> x;
	vector <int> lis(1000000, 1);
	int xi;
	while(cin >> xi)
		x.push_back(xi);

	vector <int> intervalo;
	vector <int> longest;
	int ans;
	for (int i = 0; i < x.size(); i++)
	{
		ans = 1;
		intervalo.clear();
		for (int j = 0; j < i; j++)
		{
			if (x[i] > x[j] && 1 + lis[j] > lis[i])
			{
				intervalo.push_back(x[j]);
				lis[i] = 1 + lis[j];
			}
		}
		intervalo.push_back(x[i]);
		if (intervalo.size() >= longest.size())
			longest = intervalo;
	}
	cout << longest.size() << endl;
	cout << "-" << endl;
	for (int i = 0; i < longest.size(); ++i)
		cout << longest[i] << endl;
	return 0;
}