#include <queue>
#include <iostream>
#include <cstdio>
using namespace std;


int main(int argc, char const *argv[])
{
	int t;
	while(scanf("%d", &t) and t != 0)
	{
		priority_queue <int, vector <int>, greater <int>> numbers;
		int n;
		for (int i = 0; i < t; ++i)
		{
			scanf("%d", &n);
			numbers.push(n);
		}
		int sum = 0;
		int a; 
		int b;
		int c;
		while(numbers.size() >= 2)
		{
			a = numbers.top();
			numbers.pop();
			b = numbers.top();
			numbers.pop();
			c = a + b;
			sum += c;
			numbers.push(c);
		}
		printf("%d\n", sum);

	}
	return 0;
}