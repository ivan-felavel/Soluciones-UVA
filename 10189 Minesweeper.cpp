#include <vector>
#include <iostream>
#include <cstring>
using namespace std;


typedef vector <int> vi;
typedef vector <vi> vii;
int main(int argc, char const *argv[])
{
	int contador=1;
	int m,n;
	while (cin>>n and n)
	{
		cin>>m;
		char pos;
		int tablero[n+2][m+2];
		int tabRespuesta [n+2][m+2];
		memset (tablero, -1, sizeof(tablero));
		memset (tabRespuesta, 0, sizeof(tabRespuesta));
		
		for(int i=1; i<=n; i++)
			for (int j = 1; j <=m; j++)
				{
					cin>>pos;
					tablero[i][j]=(int)pos;
				}



		for(int i=1; i<=n; i++)
			for (int j = 1; j <=m; j++)
				if(tablero[i][j]==42)
				{
					tabRespuesta[i][j]=-1;
					tabRespuesta[i+1][j+1]!=-1 ? tabRespuesta[i+1][j+1]++ : 1 ; //Arriba
					tabRespuesta[i][j+1]!=-1 ? tabRespuesta[i][j+1]++ : 1;	//Arriba derecha
					tabRespuesta[i+1][j]!= -1 ? tabRespuesta[i+1][j]++ : 1;	//Derecha
					tabRespuesta[i+1][j-1] != -1 ? tabRespuesta[i+1][j-1]++ : 1; //Abajo derecha
					tabRespuesta[i][j-1]!= -1 ? tabRespuesta[i][j-1]++ : 1;	//Abajo
					tabRespuesta[i-1][j-1] != -1 ? tabRespuesta[i-1][j-1]++ : 1; //Abajo izquierda
					tabRespuesta[i-1][j] != -1 ? tabRespuesta[i-1][j]++ : 1; //Izquierda
					tabRespuesta[i-1][j+1] != -1 ? tabRespuesta[i-1][j+1]++ : 1; //Arriba izquierda	
				}

		if (contador>1) cout<<"\n";
		cout<<"Field #"<<contador<<":"<<endl;
		contador++;
		for(int i=1; i<=n; i++)
		{
			for (int j = 1; j <=m; j++)
			{
				if(tabRespuesta[i][j]==-1)
					cout<<"*";
				else
					cout<<tabRespuesta[i][j];
			}
			cout<<endl;
		}

	}
	return 0;
}