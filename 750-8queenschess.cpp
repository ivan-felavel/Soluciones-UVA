#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
using namespace std;

int row[8], a, b, lineCounter;

bool colocar(int r, int c)
{
	for (int reina_anterior = 0; reina_anterior < c; reina_anterior++)
		if (row[reina_anterior] == r || (abs(row[reina_anterior] - r) == abs(reina_anterior - c)))
			return false; // comparten la misma fila o misma diagonal;
		return true;
}

void backtrack(int c)
{
	if (c == 8 && row[b] == a) // sol candidata, (a, b) tienen una reina
	{
		printf("%2d      %d",++lineCounter, row[0] + 1);
		for (int j = 1; j < 8; j++) printf(" %d", row[j] + 1);
		printf("\n");
	}
	for (int r = 0; r < 8; ++r) // pruebo todas las posibles filas
	{
		if (colocar(r, c)) //si puedo poner una reina en esta columna y fila
		{
			row[c] = r; 
			backtrack(c + 1); // poner esta reina aquí y hacer recursión
		}
	}
}

int main(int argc, char const *argv[])
{
	int T;
	cin >> T;
	while(T--)
	{
		cin >> a >> b;
		--a; --b;
		memset(row, 0, sizeof row);
		lineCounter = 0;
		printf("SOLN       COLUMN\n");
		printf(" #      1 2 3 4 5 6 7 8\n\n");
		backtrack(0);
		if (T) printf("\n");
	}
	return 0;
}